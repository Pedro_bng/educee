import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  ecole = {
    nom : "",
    email : "",
    telephone : "",
    ifu : "235553",
    directeur : "",
    localisation : "",
    statut : "",
  };

  eleve = {
    nom : "",
    prenoms : "",
    matricule : "",
    nom_ecole : "",
    classe : "",
  };

  parent = {
    email : "",
    password : ""
  }
  enfant: any;
  nom_enfant: any;
  prenoms_enfant: any;
  classe_enfant : any;
  ecole_enfant : any;
  

  constructor(
    private router : Router,
    private platform : Platform,
    private firestore : AngularFirestore,
    private auth : AngularFireAuth,
    

  ) {

    this.initializeApp();
    // this.enfant = localStorage.getItem("enfant");
    // this.enfant = JSON.parse(this.enfant)
    this.nom_enfant = "";
    this.prenoms_enfant = "";
    this.classe_enfant = "";
    this.ecole_enfant ="";
    
    // console.log(this.enfant)
    
  }

  onMenuOpen(){
    this.enfant = localStorage.getItem("enfant");
    this.enfant = JSON.parse(this.enfant)
    console.log(this.enfant)
    this.nom_enfant = this.enfant.nom;
    this.prenoms_enfant = this.enfant.prenoms;
    this.classe_enfant = this.enfant.classe;
    this.ecole_enfant = this.enfant.nom_ecole;

  }

  initializeApp(){
    this.platform.ready().then(()=>{
      this.auth.authState.subscribe(auth => {
        if(!auth){
          // alert("Aucun user")
           this.router.navigateByUrl('/connexion')
        } else {
          // alert("Un user")
          // console.log(auth.email)
          // var email : any = auth.email
          // console.log(email)
          // localStorage.setItem("parent_info", email)
          this.router.navigateByUrl('/choix-compte')
        }
      })
      
    })
  }
  changerCompte(){
    localStorage.removeItem('enfant');
    this.router.navigate(["/choix-compte"]);
    console.log("Afficher enfnt")
    console.log(this.enfant)
  }

   logOut(){
    this.auth.signOut().then(() =>{
      // localStorage.removeItem('parent_info');
      localStorage.clear()
      this.router.navigate(['/connexion']);
      // console.log(parent)

    });

   }
}
