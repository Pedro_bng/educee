import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommentMarchePageRoutingModule } from './comment-marche-routing.module';

import { CommentMarchePage } from './comment-marche.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommentMarchePageRoutingModule
  ],
  declarations: [CommentMarchePage]
})
export class CommentMarchePageModule {}
