import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommentMarchePage } from './comment-marche.page';

const routes: Routes = [
  {
    path: '',
    component: CommentMarchePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommentMarchePageRoutingModule {}
