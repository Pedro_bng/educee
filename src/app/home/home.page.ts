import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
// import { Routes} from '@angular/core';
// import { TabsPage } from './tabs-page';
import { App } from '@capacitor/app';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { exit } from 'process';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  niveau : string;
  url : any;
  enfant: any;
  notes: any[] = [];
  matieres = [
    'FRANCAIS',
    'MATHEMATIQUES',
    'SVT',
    'HISTOIRE-GEOGRAPHIE',
    'PHILOSOPHIE',
    'ANGLAIS',
    'PCT'
  ] ;
  context: any;

  exitCount = 0;
  backButtonSubscription : any;


  constructor(
    private auth: AngularFireAuth,
    private router : Router,
    public platform : Platform,
    private navController : NavController,
    private firestore : AngularFirestore

  ) {
    // this.initializeBackButtonCusomHandler();

    // localStorage.setItem("enfant", JSON.stringify(item))
    this.enfant = localStorage.getItem("enfant");
    this.enfant = JSON.parse(this.enfant)
    console.log(this.enfant)
    this.recuperationNotes();
    this.niveau = 'interro'
    // this.matiere = 'francais'
    this.url = router.url;

    this.platform.backButton.subscribeWithPriority(-1,()=>{
      
      if(router.url === this.url){
        App.exitApp();
      }
    })
  }

  // initializeBackButtonCusomHandler(): void {
  //   // this.exitCount = 0;
  //   this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(9999, () => {
  //     if(this.exitCount === 0) {
  //       this.exitCount++;
  //       console.log('press again to axit app')
  //       setTimeout(() => { this.exitCount = 0;}, 2000);
  //     } else {
  //       App.exitApp();
  //     }
  //   });
  // }

  ngOnDestroy(){
    this.backButtonSubscription.unsubscribe();
  }

  recuperationNotes(){

    this.firestore.firestore.collection("notes").where("id_eleve", "==", this.enfant.id_eleve).get().then(
      (reponse) => {
        if(reponse.empty){
          console.log("Aucune note")
        }else {
          this.notes = []
          reponse.forEach(doc =>{
            this.notes.push({
              matiere : doc.data()['matiere'],
              type_evaluation : doc.data()['type_evaluation'],
              valeur : doc.data()['valeur']
            })
            console.log(this.notes)
          })
        }
      }
    )
  }

}
