import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-devoirs',
  templateUrl: './devoirs.page.html',
  styleUrls: ['./devoirs.page.scss'],
})
export class DevoirsPage implements OnInit {
  // matiere : string;
  enfant : any;
  notes: any[] = [];
  matieres = [
    'FRANCAIS',
    'MATHEMATIQUES',
    'SVT',
    'HISTOIRE-GEOGRAPHIE',
    'PHILOSOPHIE',
    'ANGLAIS',
    'PCT'
  ] ;
  context: any;

  constructor(
    private auth : AngularFireAuth,
    private router : Router,
    private platform : Platform,
    private firestore : AngularFirestore,
    private navController : NavController,
  ) {
    // this.matiere = 'francais'
    this.enfant = localStorage.getItem("enfant");
    this.enfant = JSON.parse(this.enfant);
    this.recuperationNotes();
   }

  ngOnInit() {
  }

  recuperationNotes(){

    this.firestore.firestore.collection("notes").where("id_eleve", "==", this.enfant.id_eleve).get().then(
      (reponse) => {
        if(reponse.empty){
          console.log("Aucune note")
        }else {
          this.notes = []
          reponse.forEach(doc =>{
            this.notes.push({
              matiere : doc.data()['matiere'],
              type_evaluation : doc.data()['type_evaluation'],
              valeur : doc.data()['valeur']
            })
            console.log(this.notes)
          })
        }
      }
    )
  }

}
