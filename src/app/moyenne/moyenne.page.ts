import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-moyenne',
  templateUrl: './moyenne.page.html',
  styleUrls: ['./moyenne.page.scss'],
})
export class MoyennePage implements OnInit {
  enfant : any;
  notes: any[] = [];
  matieres = [
    'FRANCAIS',
    'MATHEMATIQUES',
    'SVT',
    'HISTOIRE-GEOGRAPHIE',
    'PHILOSOPHIE',
    'ANGLAIS',
    'PCT'
  ] ;
  context: any;


  constructor(
    private router : Router,
    private auth : AngularFireAuth,
    private firestore : AngularFirestore,
    private platform : Platform
  ) { 
    // this.matiere = 'francais'
    this.enfant = localStorage.getItem("enfant");
    this.enfant = JSON.parse(this.enfant);
    this.recuperationNotes();

  }

  ngOnInit() {
  }

  recuperationNotes(){

    this.firestore.firestore.collection("notes").where("id_eleve", "==", this.enfant.id_eleve).get().then(
      (reponse) => {
        if(reponse.empty){
          console.log("Aucune note")
        }else {
          this.notes = []
          reponse.forEach(doc =>{
            this.notes.push({
              matiere : doc.data()['matiere'],
              type_evaluation : doc.data()['type_evaluation'],
              valeur : doc.data()['valeur']
            })
            console.log(this.notes)
          })
        }
      }
    )
  }

}
