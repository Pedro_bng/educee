import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
// import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
// import { doc } from 'firebase/firestore';


@Component({
  selector: 'app-choix-compte',
  templateUrl: './choix-compte.page.html',
  styleUrls: ['./choix-compte.page.scss'],
})
export class ChoixComptePage implements OnInit {
  email_parent: string | null;
  nom_parent : string | undefined;
  url : any;

  list_enfant : any[] = []

  constructor(
    private router : Router,
    private auth : AngularFireAuth,
    private firestore : AngularFirestore,
    public platform : Platform,
    private controller : NavController
  ) {
    this.email_parent = localStorage.getItem('parent_info');
    // this.nom = localStorage.getItem('parent_info');
    console.log(this.email_parent)
    this.recuperation_enfants();

    this.url = router.url;

    // this.platform.backButton.subscribeWithPriority(-1,()=>{
      
    //   if(router.url === this.url){
    //     App.exitApp();
    //   }
    // })

    
  }

  ngOnInit() {
  }

  recuperation_enfants(){
    this.firestore.firestore.collection("eleves").where("email_parent", "==", this.email_parent ).get().then(
      (enfants) => {
        if(enfants.empty){
          console.log("Liste vide")
        } else {
          console.log("Wainttt")
          enfants.forEach(doc => {
            this.list_enfant.push({
              nom : doc.data()['nom'],
              prenoms : doc.data()['prenoms'],
              matricule : doc.data()['matricule'],
              nom_ecole : doc.data()['nom_ecole'],
              classe : doc.data()['classe'],
              id_eleve : doc.id
            })
          })
        }
      }
    )
  }

  recuperation_parent(){
    this.firestore.firestore.collection("parents").where("email_parent", "==", this.email_parent).get().then(
      reponse => {
        if(reponse.empty){

        } else {
          // this.nom_parent : doc.data()
        }
      }
    )
  }

  choix_compte(item : any){
    console.log(item)
    localStorage.setItem("enfant", JSON.stringify(item))
    this.router.navigate(["tabs/home"]);
  }
}
