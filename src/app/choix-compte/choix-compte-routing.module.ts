import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChoixComptePage } from './choix-compte.page';

const routes: Routes = [
  {
    path: '',
    component: ChoixComptePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChoixComptePageRoutingModule {}
