import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import "firebase/firestore" ;
import "firebase/auth";
import firebase from 'firebase/compat/app';


var firebaseConfig = {
  apiKey: "AIzaSyC5UwoA3opMbxXnBQ-NJpJiR1aG1Y3NLL8",
  authDomain: "educee-3c240.firebaseapp.com",
  projectId: "educee-3c240",
  storageBucket: "educee-3c240.appspot.com",
  messagingSenderId: "23678970918",
  appId: "1:23678970918:web:4591c3adee7774ea038ae7",
  measurementId: "G-91X7VPE394"
};

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {

  eleve = {
    nom : "",
    prenoms : "",
    matricule : "",
    nom_ecole : "",
    classe : "",
  };
  parent = {
    email : "",
    password : ""
  }

  constructor(
    private router : Router,
    private auth : AngularFireAuth,
    private firestore : AngularFirestore,

  ) { }

  ngOnInit() {
    // firebase.initializeApp(firebaseConfig);
    // firebase.initializeApp(firebaseConfig)
  }

  connexion(){
    // console.log(this.eleve.matricule);
    this.firestore.firestore.collection("parents").where("email", "==", this.parent.email).get().then(
      reponse => {
        if(reponse.empty){
          console.log("Vous n'avez pas d'enfant dans le système avec ce mail")
        }
        if(!reponse.empty){
          console.log("Welcome")
          firebase.auth().signInWithEmailAndPassword(this.parent.email, this.parent.password).then((confirmationResult) => {
            localStorage.setItem('verificationId', JSON.stringify(confirmationResult));
              // this.router.navigate(['/home']);

            this.firestore.firestore.collection('parents').where("email", "==", this.parent.email).get().then(
              reponse2 => {
              localStorage.setItem('parent_info', this.parent.email);
              console.log(this.parent.email)
              this.router.navigate(['/choix-compte']);
              }
            )
          })
        }
      }
    )
  }
}
