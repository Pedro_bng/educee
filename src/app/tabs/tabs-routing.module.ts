import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children : [
      {
        path : 'home',
        children : [
          {
            path : '',
            loadChildren : () => import('../home/home.module').then( m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'devoirs',
        children : [
          {
            path : '',
            loadChildren : () => import('../devoirs/devoirs.module').then(m => m.DevoirsPageModule)
          }
        ]
      },
      {
        path: 'moyenne',
        children : [
          {
            path : '',
            loadChildren : () => import('../moyenne/moyenne.module').then(m => m.MoyennePageModule)
          }
        ]
      },
      {
        path : '',
        redirectTo :  '/tabs/home',
        pathMatch : 'full'
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
