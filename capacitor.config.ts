import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.educee.app',
  appName: 'educee',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
